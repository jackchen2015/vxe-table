module.exports = {
  app: {
    header: {
      desc: 'A more powerful, extensible, configurable Vue Table component.'
    },
    aside: {
      nav: {
        start: 'Development',
        install: 'Install',
        use: 'Quick start',
        advancedFunctions: 'Advanced Functions',

        basics: 'Basics',
        base: 'Basics table',
        size: 'Size',
        ellipsis: 'Ellipsis',
        stripe: 'Striped',
        border: 'Border',
        style: 'Cell style',
        hideHead: 'Hidden header',
        resizable: 'Resizable',
        fluidHeight: 'Fluid-height',
        resize: 'Resize height and width',
        height: 'Table with fixed header',
        fixed: 'Table with fixed column',
        fullFixed: 'Table with fixed columns and header ',
        group: 'Grouping table head',
        seq: 'Table sequence',
        radio: 'Radio',
        checkbox: 'Checkbox',
        sort: 'Sorting',
        filter: 'Filter',
        empty: 'Empty data',
        loading: 'Loading',
        format: 'Format content',

        more: 'More',
        events: '事件绑定',
        template: '自定义模板',
        span: 'Rowspan and colspan',
        footer: 'Footer summary',
        customs: 'Show/hide columns',
        export: 'Export CSV',
        contextMenu: 'Context menu',
        expandRow: 'Expandable row',
        toolbar: 'Toolbar',
        search: 'Table search',
        pager: 'Pager',

        tree: 'Tree table',
        edit: '可编辑',
        crudToolbar: 'CRUD + Toolbar',
        full: '完整功能',

        grid: 'Grid table',
        proxy: 'DataProxy',
        proxyPage: 'DataProxy + Pager',
        fullQuery: '完整查询',
        dynamicColumn: '实现可配置动态列',
        baseTree: 'Basics tree',
        crudTreeToolbar: 'Tree + CRUD + Toolbar',

        scroll: 'Big table',
        bigData: 'Big data',
        big1wRow: '10,000 row',
        big10wRow: '100,000 row, More complex rendering',
        big1wRow1wCol: '10,000 row 10,000 column',
        big10wRow1wCol: '100,000 row 10,000 column, More complex rendering',
        infiniteScroll: 'The infinite scroll',

        editable: 'Editable',
        manual: 'Manual trigger',
        click: 'Click trigger',
        dblclick: 'dblclick trigger',
        autoClear: 'Auto clear',
        insert: 'Insert',
        delete: 'Delete',
        revert: 'Revert',
        status: 'Status',
        cellDisable: 'Disable edit cell',
        rowDisable: 'Disable edit row',
        cellVaild: 'Validate call',
        rowVaild: 'Validate row',
        keyboard: 'Keyboard navigation',

        excel: 'Excel table',
        cell: 'Cell',

        other: 'Other rendering',
        elementRender: '使用 element-ui 渲染列',
        iviewRender: '使用 iview 渲染列',
        antd: '使用 ant-design-vue 渲染列',
        sortablejsRender: '使用 sortablejs 拖拽行排序',
        xlsxRender: '使用 xlsx 导出数据',

        plugin: 'Plugins',
        elementPlugin: 'element-ui 适配插件',
        elementPluginMore: 'element-ui 跟多配置',
        iviewPlugin: 'iview 适配插件',
        iviewPluginMore: 'iview 跟多配置',
        antdPlugin: 'ant-design-vue 适配插件',
        antdPluginMore: 'ant-design-vue 跟多配置',

        api: 'API Document',
        vxeTable: 'vxe-table',
        vxeTableColumn: 'vxe-table-column',
        vxeGrid: 'vxe-grid',
        vxeExcel: 'vxe-excel',
        vxeToolbar: 'vxe-toolbar',
        vxePager: 'vxe-pager',
        vxeRadio: 'vxe-radio',
        vxeCheckbox: 'vxe-checkbox',
        vxeInput: 'vxe-input',
        vxeButton: 'vxe-button',
        vxeAlert: 'vxe-alert',
        vxeTooltip: 'vxe-tooltip'
      }
    },
    body: {
      button: {
        viewCode: 'View source',
        runDemo: 'Run demo',
        showCode: 'Show code'
      }
    }
  }
}
