module.exports = {
  vxe: {
    table: {
      emptyText: 'No Data',
      confirmFilter: 'Confirm',
      resetFilter: 'Reset',
      allFilter: 'All'
    },
    grid: {
      selectOneRecord: 'Please choose at least one piece of record!',
      removeSelectRecord: 'Are you sure you want to delete the selected record?',
      dataUnchanged: 'Data unchanged! '
    },
    pager: {
      goto: 'Go to',
      pagesize: '/page',
      total: 'Total {{total}} record',
      pageClassifier: ''
    },
    alert: {
      title: 'Message notification'
    },
    button: {
      confirm: 'Confirm',
      cancel: 'Cancel'
    }
  }
}
